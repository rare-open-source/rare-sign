import { Img, Link, Section, Text } from '../components';

export type TemplateFooterProps = {
  isDocument?: boolean;
};

export const TemplateFooter = ({ isDocument = true }: TemplateFooterProps) => {
  return (
    <Section>
      <Img
        src={'https://pkfhqkh2utuopdc8.public.blob.vercel-storage.com/logo-icon-email.png'}
        alt="Rare Logo"
        className="mx-auto mb-4 mt-6 h-6"
      />

      <Text className="text-center">© Rare Language Services.</Text>

      {isDocument && (
        <Text className="my-4 text-center text-base text-[#1E2310]">
          This document was sent using{' '}
          <Link className="text-[#747D55]" href="https://documen.so/mail-footer">
            Documenso.
          </Link>
        </Text>
      )}
    </Section>
  );
};

export default TemplateFooter;
