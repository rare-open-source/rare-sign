import { RECIPIENT_ROLES_DESCRIPTION } from '@documenso/lib/constants/recipient-roles';
import type { RecipientRole } from '@documenso/prisma/client';
import config from '@documenso/tailwind-config';

import {
  Body,
  Container,
  Head,
  Hr,
  Html,
  Img,
  Link,
  Preview,
  Section,
  Tailwind,
  Text,
} from '../components';
import type { TemplateDocumentInviteProps } from '../template-components/template-document-invite';
import { TemplateDocumentInvite } from '../template-components/template-document-invite';
import { TemplateFooter } from '../template-components/template-footer';

export type DocumentInviteEmailTemplateProps = Partial<TemplateDocumentInviteProps> & {
  customBody?: string;
  role: RecipientRole;
};

export const DocumentInviteEmailTemplate = ({
  inviterName = 'Lucas Smith',
  inviterEmail = 'lucas@documenso.com',
  documentName = 'Open Source Pledge.pdf',
  signDocumentLink = 'https://documenso.com',
  assetBaseUrl = 'http://localhost:3002',
  customBody,
  role,
}: DocumentInviteEmailTemplateProps) => {
  const action = RECIPIENT_ROLES_DESCRIPTION[role].actionVerb.toLowerCase();

  const previewText = `${inviterName} has invited you to ${action} ${documentName}`;

  const getAssetUrl = (path: string) => {
    return new URL(path, assetBaseUrl).toString();
  };

  return (
    <Html>
      <Head />
      <Preview>{previewText}</Preview>
      <Tailwind
        config={{
          theme: {
            extend: {
              colors: config.theme.extend.colors,
            },
          },
        }}
      >
        <Body className="mx-auto my-auto bg-[#F5F6F4] font-sans">
          <Section>
            <Container className="mx-auto mb-2 mt-8 max-w-xl rounded-lg p-4 backdrop-blur-sm">
              <Section>
                <Img
                  src={
                    'https://pkfhqkh2utuopdc8.public.blob.vercel-storage.com/logo-wordmark-email.png'
                  }
                  alt="Rare Logo"
                  className="mb-4 h-10"
                />

                <TemplateDocumentInvite
                  inviterName={inviterName}
                  inviterEmail={inviterEmail}
                  documentName={documentName}
                  signDocumentLink={signDocumentLink}
                  assetBaseUrl={assetBaseUrl}
                  role={role}
                />
              </Section>
            </Container>

            {customBody && (
              <Container className="mx-auto mt-12 max-w-xl">
                <Section>
                  <Text className="my-4 text-base font-semibold">
                    {inviterName}{' '}
                    <Link className="font-normal text-[#1E2310]" href="mailto:{inviterEmail}">
                      ({inviterEmail})
                    </Link>
                  </Text>

                  <Text className="mt-2 text-base text-[#1E2310]">
                    {customBody ? (
                      <pre className="text--[#1E2310] font-sans text-base">"{customBody}"</pre>
                    ) : (
                      `${inviterName} has invited you to ${action} the document "${documentName}".`
                    )}
                  </Text>
                </Section>
              </Container>
            )}

            <Hr className="mx-auto mt-12 max-w-xl" />

            <Container className="mx-auto max-w-xl">
              <TemplateFooter />
            </Container>
          </Section>
        </Body>
      </Tailwind>
    </Html>
  );
};

export default DocumentInviteEmailTemplate;
