'use client';

import config from '@documenso/tailwind-config';

import {
  Body,
  Button,
  Container,
  Head,
  Hr,
  Html,
  Img,
  Preview,
  Section,
  Tailwind,
  Text,
} from '../components';
import { TemplateFooter } from '../template-components/template-footer';
import TemplateImage from '../template-components/template-image';

export type TeamInviteEmailProps = {
  assetBaseUrl: string;
  baseUrl: string;
  senderName: string;
  teamName: string;
  teamUrl: string;
  token: string;
};

export const TeamInviteEmailTemplate = ({
  assetBaseUrl = 'http://localhost:3002',
  baseUrl = 'https://documenso.com',
  senderName = 'John Doe',
  teamName = 'Team Name',
  teamUrl = 'demo',
  token = '',
}: TeamInviteEmailProps) => {
  const previewText = `Accept invitation to join a team on Rare Sign`;

  return (
    <Html>
      <Head />
      <Preview>{previewText}</Preview>
      <Tailwind
        config={{
          theme: {
            extend: {
              colors: config.theme.extend.colors,
            },
          },
        }}
      >
        <Body className="mx-auto my-auto font-sans">
          <Section className="bg-white text-slate-500">
            <Container className="mx-auto mb-2 mt-8 max-w-xl rounded-lg  border-slate-200 p-2 backdrop-blur-sm">
              <Img
                src={
                  'https://pkfhqkh2utuopdc8.public.blob.vercel-storage.com/logo-wordmark-email.png'
                }
                alt="Rare Logo"
                className="mb-10 h-10"
              />

              <Section className="pt-8">
                <TemplateImage
                  className="mx-auto"
                  assetBaseUrl={assetBaseUrl}
                  staticAsset="add-user.png"
                />
              </Section>

              <Section className="p-2 text-slate-500">
                <Text className="text-center text-lg font-medium text-black">Join {teamName}</Text>

                <Text className="my-1 text-center text-base">
                  You have been invited to join the following team
                </Text>

                <div className="mx-auto my-2 w-fit rounded-lg bg-gray-50 px-4 py-1 text-base font-medium text-slate-600">
                  {
                    //formatTeamUrl(teamUrl, baseUrl)
                    teamUrl
                  }
                </div>

                <Section className="mb-6 mt-6 text-center">
                  <Button
                    className="inline-flex items-center justify-center rounded-lg bg-[#747D55] px-6 py-3 text-center text-sm font-medium text-white no-underline"
                    href={`${baseUrl}/team/invite/${token}`}
                  >
                    Accept
                  </Button>
                </Section>
              </Section>
            </Container>

            <Hr className="mx-auto mt-12 max-w-xl" />

            <Container className="mx-auto max-w-xl">
              <TemplateFooter isDocument={false} />
            </Container>
          </Section>
        </Body>
      </Tailwind>
    </Html>
  );
};

export default TeamInviteEmailTemplate;
