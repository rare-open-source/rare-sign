#!/bin/sh

set -x

npx prisma migrate deploy --schema ./packages/prisma/schema.prisma
npx --yes playwright install --with-deps
node apps/web/server.js
